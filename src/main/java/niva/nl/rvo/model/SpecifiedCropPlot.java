/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.List;
/*
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
*/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "StartDateTime",
    "EndDateTime",
    "AreaMeasure",
    "SpecifiedReferencedLocation",
    "GrownFieldCrop"
})
public class SpecifiedCropPlot {

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("StartDateTime")
    private String startDateTime;
    @JsonProperty("EndDateTime")
    private String endDateTime;
    @JsonProperty("AreaMeasure")
    private Double areaMeasure;
    @JsonProperty("SpecifiedReferencedLocation")
    private SpecifiedReferencedLocation specifiedReferencedLocation;
    @JsonProperty("GrownFieldCrop")
    private List<GrownFieldCrop> grownFieldCrop = null;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("StartDateTime")
    public String getStartDateTime() {
        return startDateTime;
    }

    @JsonProperty("StartDateTime")
    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    @JsonProperty("EndDateTime")
    public String getEndDateTime() {
        return endDateTime;
    }

    @JsonProperty("EndDateTime")
    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    @JsonProperty("AreaMeasure")
    public Double getAreaMeasure() {
        return areaMeasure;
    }

    @JsonProperty("AreaMeasure")
    public void setAreaMeasure(Double areaMeasure) {
        this.areaMeasure = areaMeasure;
    }

    @JsonProperty("SpecifiedReferencedLocation")
    public SpecifiedReferencedLocation getSpecifiedReferencedLocation() {
        return specifiedReferencedLocation;
    }

    @JsonProperty("SpecifiedReferencedLocation")
    public void setSpecifiedReferencedLocation(SpecifiedReferencedLocation specifiedReferencedLocation) {
        this.specifiedReferencedLocation = specifiedReferencedLocation;
    }

    @JsonProperty("GrownFieldCrop")
    public List<GrownFieldCrop> getGrownFieldCrop() {
        return grownFieldCrop;
    }

    @JsonProperty("GrownFieldCrop")
    public void setGrownFieldCrop(List<GrownFieldCrop> grownFieldCrop) {
        this.grownFieldCrop = grownFieldCrop;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
