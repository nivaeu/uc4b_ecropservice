/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "ProductName",
    "TypeCode",
    "SizeMeasure",
    "WeightMeasure",
    "UnitQuantity",
    "AppliedTreatment"
})
public class SpecifiedProductBatch {

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("ProductName")
    private String productName;
    @JsonProperty("TypeCode")
    private String typeCode;
    @JsonProperty("SizeMeasure")
    private Integer sizeMeasure;
    @JsonProperty("WeightMeasure")
    private Integer weightMeasure;
    @JsonProperty("UnitQuantity")
    private Integer unitQuantity;
    @JsonProperty("AppliedTreatment")
    private String appliedTreatment;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("ProductName")
    public String getProductName() {
        return productName;
    }

    @JsonProperty("ProductName")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @JsonProperty("TypeCode")
    public String getTypeCode() {
        return typeCode;
    }

    @JsonProperty("TypeCode")
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @JsonProperty("SizeMeasure")
    public Integer getSizeMeasure() {
        return sizeMeasure;
    }

    @JsonProperty("SizeMeasure")
    public void setSizeMeasure(Integer sizeMeasure) {
        this.sizeMeasure = sizeMeasure;
    }

    @JsonProperty("WeightMeasure")
    public Integer getWeightMeasure() {
        return weightMeasure;
    }

    @JsonProperty("WeightMeasure")
    public void setWeightMeasure(Integer weightMeasure) {
        this.weightMeasure = weightMeasure;
    }

    @JsonProperty("UnitQuantity")
    public Integer getUnitQuantity() {
        return unitQuantity;
    }

    @JsonProperty("UnitQuantity")
    public void setUnitQuantity(Integer unitQuantity) {
        this.unitQuantity = unitQuantity;
    }

    @JsonProperty("AppliedTreatment")
    public String getAppliedTreatment() {
        return appliedTreatment;
    }

    @JsonProperty("AppliedTreatment")
    public void setAppliedTreatment(String appliedTreatment) {
        this.appliedTreatment = appliedTreatment;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
