/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.internal;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import niva.nl.rvo.core.NivaECropProperties;
import niva.nl.rvo.core.UniformTimestamp;

/**
 * This class implements the client that connects to the (backend) service.
 *
 * @author rande001, rivie003
 */
public class PABackendService {
    /**
     * Call PA backend service. Just a Stub...
     *
     * @param data
     * @return
     */
    public String process(JSONObject data) {
        String backendResult;
        try {
            backendResult = callService(data.toJSONString());
            //System.out.println(backendResult);
            //
            // process the results
            JSONParser parser = new JSONParser();
            JSONObject parsedOutput = (JSONObject)parser.parse(backendResult);
            JSONArray result = (JSONArray)((JSONObject)parsedOutput.get("ECROPReturnMessage")).get("Messages");
        } catch (Exception e) {
            throw new RuntimeException("PABackendService.process " + e.getLocalizedMessage());
        }
        return backendResult;
    }

    /**
     * Call the internal PA service.
     *
     * @param inputJson
     * @return
     */
    private String callService(String inputJson) {
        String urlString = "";
        try {
            urlString = NivaECropProperties.getStringProperty("internalservice.url");
            if (urlString.isEmpty()) {
                return "{\"ECROPReturnMessage\":" +
" {\"ReturnCode\": 0," +
"  \"ReturnMessage\": \"Ok (no backend url)\"," +
"  \"MessageCount\": 0," +
"  \"TimeStamp\": \"" + UniformTimestamp.getUniformTimestamp() + "\"" +
" }" +
"}";
            }
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(5000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            
            OutputStream os = connection.getOutputStream();
            os.write(inputJson.getBytes("UTF-8"));
            os.close();

            InputStream is = (InputStream) connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder result = new StringBuilder();
            String line = null;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            is.close();
            return result.toString();
        } catch (Exception e) {
            System.out.println("------------- START ---------------");
            System.out.println("Sending json " + inputJson);
            System.out.println("");
            System.out.println("URL " + urlString);
            System.out.println("");
            System.out.println("Message ".concat(e.getLocalizedMessage()));
            throw new RuntimeException("callService: ".concat(e.getLocalizedMessage()));
        }
    }
}
