/* 
 * Create niva database user, database
 * Usage: psql -U postgres -h scomp1271.wurnet.nl -f 00-niva-database.sql 
 */
 
DROP DATABASE IF EXISTS niva;

DROP USER IF EXISTS niva;

CREATE DATABASE niva ENCODING = 'UTF8';

CREATE USER niva WITH PASSWORD 'forn1v@';

ALTER DATABASE niva OWNER TO niva;

GRANT ALL PRIVILEGES ON DATABASE niva to postgres;
GRANT ALL PRIVILEGES ON DATABASE niva to niva;
