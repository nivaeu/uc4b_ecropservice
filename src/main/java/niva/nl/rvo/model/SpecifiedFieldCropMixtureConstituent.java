/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

/*
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
*/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CropProportionPercent",
    "SpecifiedBotanicalCrop"
})
public class SpecifiedFieldCropMixtureConstituent {

    @JsonProperty("CropProportionPercent")
    private Double cropProportionPercent;
    @JsonProperty("SpecifiedBotanicalCrop")
    private SpecifiedBotanicalCrop specifiedBotanicalCrop;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CropProportionPercent")
    public Double getCropProportionPercent() {
        return cropProportionPercent;
    }

    @JsonProperty("CropProportionPercent")
    public void setCropProportionPercent(Double cropProportionPercent) {
        this.cropProportionPercent = cropProportionPercent;
    }

    @JsonProperty("SpecifiedBotanicalCrop")
    public SpecifiedBotanicalCrop getSpecifiedBotanicalCrop() {
        return specifiedBotanicalCrop;
    }

    @JsonProperty("SpecifiedBotanicalCrop")
    public void setSpecifiedBotanicalCrop(SpecifiedBotanicalCrop specifiedBotanicalCrop) {
        this.specifiedBotanicalCrop = specifiedBotanicalCrop;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
