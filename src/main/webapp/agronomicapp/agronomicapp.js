"use strict";
/* agronomicapp4niva */
var map, layerManager, spin, niva;
function LayerManager(map){
    this.map = map;
    this.list = {};

    var google = L.tileLayer(window.location.protocol +'//{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
        maxNativeZoom: 20,	// map zooming can go up to 22, auto-scaling tiles
        maxZoom: 22,
        attribution:'&copy; <a href="https://www.google.com/intl/en/help/terms_maps.html" target="_blank" title="Map data © Google 2017, Imagery © CNES / Airbus, DigitalGlobe, U.S. Geological Survey, USDA Farm Service Agency 2017">Google</a>',
        subdomains:['mt0','mt1','mt2','mt3']
    });
    var osm = L.tileLayer(window.location.protocol+ '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright" target="_blank">OpenStreetMap</a> contributors'
    });
    this.map.addLayer(google);

    var baseLayers = {
        "Google Hybride": google,
        "OpenStreetMap":  osm
    };
    this.controlLayers = L.control.layers(baseLayers,null,{position:'bottomright'}).addTo(this.map);
    
    this.AddOverlay = function(layer, name) {
		if (this.list[name]) {
			if (this.list[name].hasOwnProperty('clearLayers'))
				this.list[name].clearLayers();
			this.controlLayers.removeLayer(this.list[name]);
			this.map.removeLayer(this.list[name])
			delete this.list[name];
		}
		
		this.list[name] = layer;
		this.controlLayers.addOverlay(layer, name);
    };   
}

function stopP(ev) {
	L.DomEvent.stopPropagation(ev);
}

function mousePropagationKiller(div){
	if (!L.Browser.touch) {
		L.DomEvent.disableClickPropagation(div);
		L.DomEvent.disableScrollPropagation(div);
		L.DomEvent.on(div, 'mousewheel', L.DomEvent.stopPropagation);
	} else {
		//L.DomEvent.on(div, 'click', L.DomEvent.stopPropagation);
		L.DomEvent.on(div, 'click dblclick', function (ev) {
			if (ev.target.localName == 'div' && !ev.bubbles) {
				L.DomEvent.stopPropagation(ev);
				L.DomEvent.stop(ev);
			}
		});
		div.addEventListener('mouseover', function () {
			map.dragging.disable();
			map.doubleClickZoom.disable()
		});
		div.addEventListener('mouseout', function () {
			map.dragging.enable();
			map.doubleClickZoom.enable()
		});
	}
}

function addControlPlaceholders(map) {
    var corners = map._controlCorners,
        l = 'leaflet-',
        container = map._controlContainer;

    function createCorner(vSide, hSide) {
        var className = l + vSide + ' ' + l + hSide;

        corners[vSide + hSide] = L.DomUtil.create('div', className, container);
    }

    createCorner('verticalcenter', 'left');
    createCorner('verticalcenter', 'right');
    createCorner('horizontalcenter', 'top');
    createCorner('horizontalcenter', 'bottom');
}

function loadApp(e) {
    if (!map){
        map = L.map('map').setView([51.99, 5.663], 5);
        layerManager = new LayerManager(map);   

	addControlPlaceholders(map);
        spin = L.control({position: 'horizontalcentertop'});
	spin.onAdd = function (map) {
			this._modal= L.DomUtil.create('div', 'modal');					// create a div with a class "modal"
			mousePropagationKiller(this._modal);
			this._div  = L.DomUtil.create('div', '', this._modal);
			return this._modal;
		};
		spin.spin = undefined;
		spin.show = function () {
			if (!this.spin)
                            this.spin = new Spinner({color:'#9c0', lines: 12, length: 30, radius: 20, width: 8}).spin(this._div);
			else
                            this.spin.spin(this._div);

			this._modal.style.display = "block";
		};
		spin.hide = function () {
			this.spin.stop();

			this._modal.style.display = "none";
		};
		spin.addTo(map);
                 
                function highlightFeature(e) {
                    var layer = e.target;
                    layer.setStyle({color: 'deeppink',fillOpacity: 1});

                    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                        layer.bringToFront();
                    }
                }
                function resetHighlight(e) {
                    var layer = e.target;
                    layer.setStyle({color: 'pink',fillOpacity: 0.5});
                }
                
		// right
		niva = L.control({position: 'topright'});
                niva.root = document.location.toString().split('/');
                niva.root = niva.root[niva.root.length-3];
                niva.onAdd = function (map) {
			this._div = L.DomUtil.create('div', 'niva'); // create a div with a class "list"
			this._head= L.DomUtil.create('h4', '', this._div);
			this._head.innerHTML = "<b>NIVA</b>";
			this._unLbl= L.DomUtil.create('label', 'padtop padleft', this._div);
			this._unLbl.innerHTML = "<i>username</i>";
			this.usern = L.DomUtil.create('input', 'padleft padbottom padright', this._div);
			this.usern.type = "text";
			this._pwLbl= L.DomUtil.create('label', 'padtop padleft', this._div);
			this._pwLbl.innerHTML = "<i>password</i>";
			this.passw = L.DomUtil.create('input', 'padleft padbottom padright', this._div);
			this.passw.type = "text";
			this._urlLbl= L.DomUtil.create('label', 'padtop padleft', this._div);
			this._urlLbl.innerHTML = "<i>url</i>";
			this.url = L.DomUtil.create('input', 'padleft padbottom padright', this._div);
			this.url.type = "text";
			this._reqLbl= L.DomUtil.create('label', 'padtop padleft', this._div);
			this._reqLbl.innerHTML = "<i>request</i>";
			// form
			this.reqForm = L.DomUtil.create('form', 'padleft', this._div);
			this.request = L.DomUtil.create('textarea', '', this.reqForm);
                        this.request.title = "ecropmessage (POST body) or asappliedmap or asappliedmap/n (REST)"
			this._reqButt = L.DomUtil.create('input', 'submit-button padright', this.reqForm);
			this._reqButt.type = 'submit';
			this._reqButt.value= ">";
			
			this._respLbl= L.DomUtil.create('label', 'padleft', this._div);
			this._respLbl.innerHTML = "<i>response</i>";
			this.response= L.DomUtil.create('textarea', 'padleft padbottom padright', this._div);
			this.response['readOnly'] = true;
 			mousePropagationKiller(this._div);
			this.reqForm.addEventListener('submit', function(e) {
				niva.update("");
				if (niva.request.value.indexOf('asappliedmap') == 0 || niva.request.value.indexOf('aam') == 0) {
					$.ajax({
						type: "GET",
						url: "/"+niva.root+"/rest/"+niva.request.value,
						contentType: "application/json",
						headers: {"Authorization": "Basic " + btoa(unescape(encodeURIComponent(niva.usern.value+":"+niva.passw.value)))},
						error: function(xhr, textStatus, errorThrown) {
							//13:58:27,029 INFO  [stdout] (default task-1) {"ECROPReturnMessage":{"ReturnCode":400,"ReturnMessage":"Error 5: PABackendService.process class org.json.simple.JSONArray cannot be cast to class org.json.simple.JSONObject (org.json.simple.JSONArray and org.json.simple.JSONObject are in unnamed module of loader 'deployment.eCropService-0.1-SNAPSHOT.war' @2d7a9b4f)","ErrorCount":1,"TimeStamp":"2020-03-24 13:58:27.0274+00 (UTC)"}}
							if (xhr.hasOwnProperty("responseJSON")) {
                                                            niva.update(xhr.responseJSON);
							}
							else {
                                                            niva.update(textStatus +': ' + (xhr.status || "") +' '+ (xhr.statusText || ""));
							}
						},
						success: function (response) {
							if (response.hasOwnProperty("asappliedmap"))
								niva.update("Success, available: " + response.asappliedmap);
							else if (response.hasOwnProperty("aam"))
								niva.update("Success, available: " + response.aam);
							else if (response.hasOwnProperty("type") && response["type"] == "FeatureCollection" && response.hasOwnProperty("features") && response.features != null) {
								niva.update("Success: FeatureCollection of length " + response.features.length);
								var alayer = L.geoJSON(response, {title: 'NIVA AAM' + (niva.request.value+"?").split("?")[1], 
									style: {color: 'pink',fillOpacity:0.5},
									pointToLayer: function (feature, latlng) {
										return L.circleMarker(latlng, {radius: 10});
									},
									onEachFeature: function (aFeature, aaLayer) {
										var d, props = "<b>"+ (aaLayer.options || aaLayer._options).title +"</b><br>";
										for (d in aFeature.properties) {
											props += '<br>' + d + ': ' + aFeature.properties[d];
										}
										aaLayer.bindTooltip(props, { direction:'center', className: 'label' });
                                                                                aaLayer.on({
                                                                                    mouseover: highlightFeature,
                                                                                    mouseout: resetHighlight
                                                                                });
                                                                                                                                                        }
								}).addTo(map);
                                                                layerManager.AddOverlay(alayer, "NIVA AAM" + (niva.request.value+"?").split("?")[1]);
                                                                map.fitBounds(alayer.getBounds(), {padding: [5,5]});

							}
							else
								niva.update("Success: " + Object.keys(response));
						}
					});
				} else if (niva.url.value == "") {
					$.ajax({
						type: "POST",
						url: "/"+niva.root+"/nivaecropmessage",
						//http://niva-ecropservice-pygaos.apps.ocp.wurnet.nl/eCropService-0.1-SNAPSHOT/nivaecropmessage
						//data: JSON.stringify(niva.request.value),
						data: niva.request.value,
						//processData: false,
						contentType: "application/json",
						headers: {"Authorization": "Basic " + btoa(unescape(encodeURIComponent(niva.usern.value+":"+niva.passw.value)))},
						error: function(xhr, textStatus, errorThrown) {
							//13:58:27,029 INFO  [stdout] (default task-1) {"ECROPReturnMessage":{"ReturnCode":400,"ReturnMessage":"Error 5: PABackendService.process class org.json.simple.JSONArray cannot be cast to class org.json.simple.JSONObject (org.json.simple.JSONArray and org.json.simple.JSONObject are in unnamed module of loader 'deployment.eCropService-0.1-SNAPSHOT.war' @2d7a9b4f)","ErrorCount":1,"TimeStamp":"2020-03-24 13:58:27.0274+00 (UTC)"}}
							if (xhr.hasOwnProperty("responseJSON") && xhr.responseJSON.hasOwnProperty("ECROPReturnMessage") && xhr.responseJSON.ECROPReturnMessage.ReturnCode) {
								niva.update(xhr.responseJSON.ECROPReturnMessage.ReturnMessage);
								var jsonData = xhr.responseJSON.ECROPReturnMessage;
							}
							else {
								niva.update(textStatus +': ' + (xhr.status || "") +' '+ (xhr.statusText || ""));
							}
						},
						success: function (response) {
							if (response.hasOwnProperty("ECROPReturnMessage") && response.ECROPReturnMessage.hasOwnProperty("ReturnCode") && response.ECROPReturnMessage.hasOwnProperty("MessageCount")) {
								if (response.ECROPReturnMessage.ReturnCode > 99 || response.ECROPReturnMessage.MessageCount > 0) {
									niva.update(response.ECROPReturnMessage.ReturnMessage +" #messages: " + response.ECROPReturnMessage.MessageCount);
								}
								else {
									niva.update(response.ECROPReturnMessage.ReturnMessage);
								}
								
								var jsonData = response.ECROPReturnMessage;
							}
							else {
								niva.update("Success but wrong format " + response);
							}
						}
					});
				} else  {
					$.ajax({
						type: "POST",
						url: niva.url.value,
						data: niva.request.value,
						contentType: "application/json",
						error: function(xhr, textStatus, errorThrown) {
							if (xhr.hasOwnProperty("responseJSON") && xhr.responseJSON.hasOwnProperty("ECROPReturnMessage") && xhr.responseJSON.ECROPReturnMessage.ReturnCode) {
								niva.update(xhr.responseJSON.ECROPReturnMessage.ReturnMessage);
								var jsonData = xhr.responseJSON.ECROPReturnMessage;
							}
							else {
								niva.update(textStatus +': ' + (xhr.status || "") +' '+ (xhr.statusText || ""));
							}
						},
						success: function (response) {
							if (response.hasOwnProperty("ECROPReturnMessage") && response.ECROPReturnMessage.hasOwnProperty("ReturnCode") && response.ECROPReturnMessage.hasOwnProperty("MessageCount")) {
								if (response.ECROPReturnMessage.ReturnCode > 99 || response.ECROPReturnMessage.MessageCount > 0) {
									niva.update(response.ECROPReturnMessage.ReturnMessage +" #messages: " + response.ECROPReturnMessage.MessageCount);
								}
								else {
									niva.update(response.ECROPReturnMessage.ReturnMessage);
								}
								
								var jsonData = response.ECROPReturnMessage;
							}
							else {
								niva.update("Success but wrong format " + response);
							}
						}
					});
				}
				e.preventDefault();
				return false; // prevent page reload
			}, false);
			return this._div;
		};
		niva.update = function (value) {
			this.response['value'] = value;
		};
		niva.addTo(map);
    }
}