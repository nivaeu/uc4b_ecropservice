/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
   "Element",
   "Severity",
   "MessageCode",
   "Message"
})
/* 
"Element": "...",
"Severity": "Error",    // can be: 'Debug', 'Info', 'Warning', 'Error'
"MessageCode": 100,
"Message": "Invalid timestamp"
*/
public class Message {
    /**
     *
     * @param element
     * @param severity
     * @param messageCode
     * @param message
     */
    @JsonCreator
    public Message(@JsonProperty(value = "Element", required = true) String element, 
                   @JsonProperty(value = "Severity", required = true) String severity, 
                   @JsonProperty(value = "MessageCode", required = true) int messageCode, 
                   @JsonProperty(value = "Message", required = true) String message) {
        this.element = element;
        this.severity = severity;
        this.messageCode = messageCode;
        this.message = message;
    }


    @JsonProperty("Element")
    private String element;
    
    @JsonProperty("Severity")
    private String severity;
    
    @JsonProperty("MessageCode")
    private Integer messageCode;

    @JsonProperty("Message")
    private String message;
    
    
    @JsonProperty("Element")
    public String getElement() {
        return element;
    }

    @JsonProperty("Element")
    public void setElement(String element) {
        this.element = element;
    }

    @JsonProperty("Severity")
    public String getSeverity() {
        return severity;
    }

    @JsonProperty("Severity")
    public void setSeverity(String severity) {
        this.severity = severity;
    }

    @JsonProperty("MessageCode")
    public Integer getMessageCode() {
        return messageCode;
    }

    @JsonProperty("MessageCode")
    public void setMessageCode(Integer messageCode) {
        this.messageCode = messageCode;
    }
    
    @JsonProperty("Message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("Message")
    public void setMessage(String message) {
        this.message = message;
    }
}
