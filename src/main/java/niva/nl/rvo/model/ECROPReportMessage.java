/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CropReportDocument",
    "AgriculturalProducerParty"
})
public class ECROPReportMessage {

    @JsonProperty("CropReportDocument")
    private CropReportDocument cropReportDocument;
    @JsonProperty("AgriculturalProducerParty")
    private AgriculturalProducerParty agriculturalProducerParty;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CropReportDocument")
    public CropReportDocument getCropReportDocument() {
        return cropReportDocument;
    }

    @JsonProperty("CropReportDocument")
    public void setCropReportDocument(CropReportDocument cropReportDocument) {
        this.cropReportDocument = cropReportDocument;
    }

    @JsonProperty("AgriculturalProducerParty")
    public AgriculturalProducerParty getAgriculturalProducerParty() {
        return agriculturalProducerParty;
    }

    @JsonProperty("AgriculturalProducerParty")
    public void setAgriculturalProducerParty(AgriculturalProducerParty agriculturalProducerParty) {
        this.agriculturalProducerParty = agriculturalProducerParty;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
