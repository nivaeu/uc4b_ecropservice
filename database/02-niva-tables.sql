/* 
 * Create tables for the niva database, niva schema
 * Usage: psql -U niva -h scomp1271.wurnet.nl -d niva -f 02-niva-tables.sql 
 */

BEGIN;

DROP TABLE IF EXISTS niva.CropReport CASCADE;

DROP SEQUENCE IF EXISTS niva.cropreport_id_seq;

CREATE TABLE niva.CropReport (
    ID serial NOT NULL,
    CreationDateTime TIMESTAMPTZ NOT NULL,
	Issuer VARCHAR NOT NULL,
	ECropReportMessage JSONB,
	ECropReturnMessage JSONB);
    
ALTER TABLE niva.CropReport ADD CONSTRAINT pk_CropReport PRIMARY KEY (ID);

CREATE INDEX idxginenvelop ON niva.CropReport USING gin ((ECropReportMessage->'ECROPReportMessage'->'CropReportDocument'));

/* 2020-06 */
CREATE INDEX idxginreturncode ON niva.CropReport USING gin (((ecropreturnmessage -> 'ECropReturnMessage'::text) -> 'ReturnCode'::text));

CREATE INDEX idxginfeature ON niva.CropReport  USING gin ((ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'->'GrownFieldCrop'->'ApplicableCropProductionAgriculturalProcess'->'AppliedSpecifiedAgriculturalApplication'->'AppliedSpecifiedAgriculturalApplicationRate'->'AppliedReferencedLocation'->'PhysicalSpecifiedGeographicalFeature') jsonb_path_ops);

CREATE INDEX idxginproductname ON niva.CropReport  USING gin ((ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'->'GrownFieldCrop'->'ApplicableCropProductionAgriculturalProcess'->'AppliedSpecifiedAgriculturalApplication'->'SpecifiedProductBatch') jsonb_path_ops);

CREATE INDEX idxginbotanicalspeciescode ON niva.CropReport  USING gin ((ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'->'GrownFieldCrop'->'SpecifiedFieldCropMixtureConstituent'->'SpecifiedBotanicalCrop') jsonb_path_ops);

END;