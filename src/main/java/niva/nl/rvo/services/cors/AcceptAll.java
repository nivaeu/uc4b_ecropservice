/*
* Copyright 2020 Wageningen Environmental Research
*
* For licensing information read the included LICENSE.txt file.
*
* Unless required by applicable law or agreed to in writing, this software
* is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF
* ANY KIND, either express or implied.
*/
package niva.nl.rvo.services.cors;

import javax.servlet.http.HttpServletResponse;

/**
 * @author rande001, rivie003
 */
public class AcceptAll {
    
     public static  void setAccessControlHeaders(HttpServletResponse resp) {
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Headers","Origin, Content-Type, Accept, Authorization, token");
        // response.getHttpHeaders().add("Access-Control-Allow-Credentials","true");
        resp.setHeader("Access-Control-Allow-Methods","GET, POST, OPTIONS");
    
        resp.setStatus(HttpServletResponse.SC_OK);
    }
}
