**this repository holds the in progress demo-1 PA webservice for uc4b**  


*  JAVA servlet war with secured endpoints:  
   *   nivaecropmessage
   *   rest/asappliedmap (old)
   *   rest/aam (new)

*nivaecropmessage:*  
*  check input empty
*  checks valid JSON
*  checks ECROPReportMessage (in object) by parsing into JAVA classes generated from sample message
*  stub validator call
*  checks validator ECROPReturnMessage (in object) by parsing into JAVA classes
*  stub backend call
*  returns ECROPReturnMessage (in object)
*  saves in single row in database table CropReport
   *  ECROPReportMessage as JSONB
   *  ECROPReturnMessage as JSONB
   *  ID as serial
   *  issuer and 
   *  TZ-datetime
* as of release 2 the received message is parsed and the elements are stored in db tables which been reated for this purpose.
*  run in wildfly/JBoss docker (Dockerfile included)
*  security constraints and roles for
   *  read-only (asappliedmap) and 
   *  read-write (nivaecropmessage)

*rest/asappliedmap and /rest/aam, without ID:*
*  uses aam_id view in database
*  returns list of asappliedmap IDs

*with ID:*
*  returns feature collection asappliedmap for an ID
*  add properties from asappliedmap view to properties in feature  
*  *rest/asappliedmap/id: (old)*
   *  uses appliedmap view in database (simple json path expressions)  
*  *rest/aam/id{?page=#}: (new)*
   *  with paging starting from 0, pagesize 500
   *  uses asappliedmap view in database (recursive json path expressions)

*test:*
*  html/javascript testpage agronomicapp (in package)
   *  POSTing JSON in body
   *  requesting aam{/id{?page=#}}
