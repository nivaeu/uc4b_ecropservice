/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.core;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author rivie003
 */
public class UniformTimestamp {
    public static String getUniformTimestamp() {
        // ISO 8601 format needs 'T'
        // https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
        String formatDateTime, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSSx";
        try {
            // "2020-08-02 05:45:12.6781+02"    <- "yyyy-MM-dd HH:mm:ss.SSSSx"
            // "2020-05-14T11:34:14.03162Z"     <- DateTimeFormatter.ISO_OFFSET_DATE_TIME
            // "2020-05-14T12:05:38.30765Z[UTC]"<- DateTimeFormatter.ISO_DATE_TIME
            // "2020-05-14T11:38:59.2500+00"    <- "yyyy-MM-dd'T'HH:mm:ss.SSSSx"
            //
            DateTimeFormatter nivaformat = DateTimeFormatter.ofPattern(pattern);  
            formatDateTime = ZonedDateTime.now().format(nivaformat);
        } catch (Exception e) {
            formatDateTime = pattern.concat(e.getMessage());
        }
        
        return formatDateTime;
    }
}