/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "SpecifiedProductBatch",
    "AppliedAgriculturalApplicationRate"
})
public class AppliedSpecifiedAgriculturalApplication {

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("SpecifiedProductBatch")
    private List<SpecifiedProductBatch> specifiedProductBatch = null;
    @JsonProperty("AppliedSpecifiedAgriculturalApplicationRate")
    private List<AppliedSpecifiedAgriculturalApplicationRate> appliedSpecifiedAgriculturalApplicationRate = null;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("SpecifiedProductBatch")
    public List<SpecifiedProductBatch> getSpecifiedProductBatch() {
        return specifiedProductBatch;
    }

    @JsonProperty("SpecifiedProductBatch")
    public void setSpecifiedProductBatch(List<SpecifiedProductBatch> specifiedProductBatch) {
        this.specifiedProductBatch = specifiedProductBatch;
    }

    @JsonProperty("AppliedSpecifiedAgriculturalApplicationRate")
    public List<AppliedSpecifiedAgriculturalApplicationRate> getAppliedSpecifiedAgriculturalApplicationRate() {
        return appliedSpecifiedAgriculturalApplicationRate;
    }

    @JsonProperty("AppliedSpecifiedAgriculturalApplicationRate")
    public void setAppliedSpecifiedAgriculturalApplicationRate(List<AppliedSpecifiedAgriculturalApplicationRate> appliedSpecifiedAgriculturalApplicationRate) {
        this.appliedSpecifiedAgriculturalApplicationRate = appliedSpecifiedAgriculturalApplicationRate;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
