/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "BotanicalIdentificationID",
    "BotanicalSpeciesCode",
    "PurposeCode",
    "BotanicalGenusCode",
    "BotanicalName",
    "SownCropSpeciesVariety"
})
public class SpecifiedBotanicalCrop {

    @JsonProperty("BotanicalIdentificationID")
    private String botanicalIdentificationID;
    @JsonProperty("BotanicalSpeciesCode")
    private String botanicalSpeciesCode;
    @JsonProperty("PurposeCode")
    private String purposeCode;
    @JsonProperty("BotanicalGenusCode")
    private String botanicalGenusCode;
    @JsonProperty("BotanicalName")
    private String botanicalName;
    @JsonProperty("SownCropSpeciesVariety")
    private List<SownCropSpeciesVariety> sownCropSpeciesVariety = null;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("BotanicalIdentificationID")
    public String getBotanicalIdentificationID() {
        return botanicalIdentificationID;
    }

    @JsonProperty("BotanicalIdentificationID")
    public void setBotanicalIdentificationID(String botanicalIdentificationID) {
        this.botanicalIdentificationID = botanicalIdentificationID;
    }

    @JsonProperty("BotanicalSpeciesCode")
    public String getBotanicalSpeciesCode() {
        return botanicalSpeciesCode;
    }

    @JsonProperty("BotanicalSpeciesCode")
    public void setBotanicalSpeciesCode(String botanicalSpeciesCode) {
        this.botanicalSpeciesCode = botanicalSpeciesCode;
    }

    @JsonProperty("PurposeCode")
    public String getPurposeCode() {
        return purposeCode;
    }

    @JsonProperty("PurposeCode")
    public void setPurposeCode(String purposeCode) {
        this.purposeCode = purposeCode;
    }

    @JsonProperty("BotanicalGenusCode")
    public String getBotanicalGenusCode() {
        return botanicalGenusCode;
    }

    @JsonProperty("BotanicalGenusCode")
    public void setBotanicalGenusCode(String botanicalGenusCode) {
        this.botanicalGenusCode = botanicalGenusCode;
    }

    @JsonProperty("BotanicalName")
    public String getBotanicalName() {
        return botanicalName;
    }

    @JsonProperty("BotanicalName")
    public void setBotanicalName(String botanicalName) {
        this.botanicalName = botanicalName;
    }

    @JsonProperty("SownCropSpeciesVariety")
    public List<SownCropSpeciesVariety> getSownCropSpeciesVariety() {
        return sownCropSpeciesVariety;
    }

    @JsonProperty("SownCropSpeciesVariety")
    public void setSownCropSpeciesVariety(List<SownCropSpeciesVariety> sownCropSpeciesVariety) {
        this.sownCropSpeciesVariety = sownCropSpeciesVariety;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
