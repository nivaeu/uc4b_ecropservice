/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "ReportCountNumeric",
    "Description",
    "IssueDateTime",
    "TypeCode",
    "CopyIndicator",
    "ControlRequirementIndicator",
    "PurposeCode",
    "LineCountNumeric",
    "Information",
    "StatusCode",
    "SequenceID",
    "SenderSpecifiedParty",
    "RecipientSpecifiedParty"
})
public class CropReportDocument {

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("ReportCountNumeric")
    private Integer reportCountNumeric;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("IssueDateTime")
    private String issueDateTime;
    @JsonProperty("TypeCode")
    private String typeCode;
    @JsonProperty("CopyIndicator")
    private String copyIndicator;
    @JsonProperty("ControlRequirementIndicator")
    private String controlRequirementIndicator;
    @JsonProperty("PurposeCode")
    private String purposeCode;
    @JsonProperty("LineCountNumeric")
    private Integer lineCountNumeric;
    @JsonProperty("Information")
    private String information;
    @JsonProperty("StatusCode")
    private String statusCode;
    @JsonProperty("SequenceID")
    private String sequenceID;
    @JsonProperty("SenderSpecifiedParty")
    private SenderSpecifiedParty senderSpecifiedParty;
    @JsonProperty("RecipientSpecifiedParty")
    private RecipientSpecifiedParty recipientSpecifiedParty;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("ReportCountNumeric")
    public Integer getReportCountNumeric() {
        return reportCountNumeric;
    }

    @JsonProperty("ReportCountNumeric")
    public void setReportCountNumeric(Integer reportCountNumeric) {
        this.reportCountNumeric = reportCountNumeric;
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("Description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("IssueDateTime")
    public String getIssueDateTime() {
        return issueDateTime;
    }

    @JsonProperty("IssueDateTime")
    public void setIssueDateTime(String issueDateTime) {
        this.issueDateTime = issueDateTime;
    }

    @JsonProperty("TypeCode")
    public String getTypeCode() {
        return typeCode;
    }

    @JsonProperty("TypeCode")
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @JsonProperty("CopyIndicator")
    public String getCopyIndicator() {
        return copyIndicator;
    }

    @JsonProperty("CopyIndicator")
    public void setCopyIndicator(String copyIndicator) {
        this.copyIndicator = copyIndicator;
    }

    @JsonProperty("ControlRequirementIndicator")
    public String getControlRequirementIndicator() {
        return controlRequirementIndicator;
    }

    @JsonProperty("ControlRequirementIndicator")
    public void setControlRequirementIndicator(String controlRequirementIndicator) {
        this.controlRequirementIndicator = controlRequirementIndicator;
    }

    @JsonProperty("PurposeCode")
    public String getPurposeCode() {
        return purposeCode;
    }

    @JsonProperty("PurposeCode")
    public void setPurposeCode(String purposeCode) {
        this.purposeCode = purposeCode;
    }

    @JsonProperty("LineCountNumeric")
    public Integer getLineCountNumeric() {
        return lineCountNumeric;
    }

    @JsonProperty("LineCountNumeric")
    public void setLineCountNumeric(Integer lineCountNumeric) {
        this.lineCountNumeric = lineCountNumeric;
    }

    @JsonProperty("Information")
    public String getInformation() {
        return information;
    }

    @JsonProperty("Information")
    public void setInformation(String information) {
        this.information = information;
    }

    @JsonProperty("StatusCode")
    public String getStatusCode() {
        return statusCode;
    }

    @JsonProperty("StatusCode")
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("SequenceID")
    public String getSequenceID() {
        return sequenceID;
    }

    @JsonProperty("SequenceID")
    public void setSequenceID(String sequenceID) {
        this.sequenceID = sequenceID;
    }

    @JsonProperty("SenderSpecifiedParty")
    public SenderSpecifiedParty getSenderSpecifiedParty() {
        return senderSpecifiedParty;
    }

    @JsonProperty("SenderSpecifiedParty")
    public void setSenderSpecifiedParty(SenderSpecifiedParty senderSpecifiedParty) {
        this.senderSpecifiedParty = senderSpecifiedParty;
    }

    @JsonProperty("RecipientSpecifiedParty")
    public RecipientSpecifiedParty getRecipientSpecifiedParty() {
        return recipientSpecifiedParty;
    }

    @JsonProperty("RecipientSpecifiedParty")
    public void setRecipientSpecifiedParty(RecipientSpecifiedParty recipientSpecifiedParty) {
        this.recipientSpecifiedParty = recipientSpecifiedParty;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
