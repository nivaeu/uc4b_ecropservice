/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.internal.test;

import niva.nl.rvo.internal.PABackendService;
import org.json.simple.JSONObject;

/**
 *
 * @author rande001, rivie003
 */
public class PABackendServiceTest {

    public static void main(String[] args) {
        new PABackendServiceTest().test179786();
    }

    public PABackendServiceTest() {
    }

    /**
     * Test of calculate method, of class RecalculateServiceSimulator. Perceel 179786 owner user 36
     */
    private void test179786() {
        PABackendService instance = new PABackendService();

        System.out.println("calculate-1");

        JSONObject testData = new JSONObject();

        String result = instance.process(testData);
    }
}
