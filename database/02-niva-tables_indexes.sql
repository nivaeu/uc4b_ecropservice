/* 
 * 20200604 (RE)Create indexes for the niva database, niva schema
 * Usage: psql -U niva -h scomp1271.wurnet.nl -d niva -f 02-niva-tables_indexes.sql 
 */

BEGIN;

DROP INDEX IF EXISTS idxginbotanicalspeciescode;

DROP INDEX IF EXISTS idxginproductname;

DROP INDEX IF EXISTS idxginfeature;

DROP INDEX IF EXISTS idxginreturncode;

CREATE INDEX idxginreturncode ON niva.CropReport USING gin (((ecropreturnmessage -> 'ECropReturnMessage'::text) -> 'ReturnCode'::text));

CREATE INDEX idxginfeature ON niva.CropReport  USING gin ((ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'->'GrownFieldCrop'->'ApplicableCropProductionAgriculturalProcess'->'AppliedSpecifiedAgriculturalApplication'->'AppliedSpecifiedAgriculturalApplicationRate'->'AppliedReferencedLocation'->'PhysicalSpecifiedGeographicalFeature') jsonb_path_ops);

CREATE INDEX idxginproductname ON niva.CropReport  USING gin ((ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'->'GrownFieldCrop'->'ApplicableCropProductionAgriculturalProcess'->'AppliedSpecifiedAgriculturalApplication'->'SpecifiedProductBatch') jsonb_path_ops);

CREATE INDEX idxginbotanicalspeciescode ON niva.CropReport  USING gin ((ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'->'GrownFieldCrop'->'SpecifiedFieldCropMixtureConstituent'->'SpecifiedBotanicalCrop') jsonb_path_ops);

END;