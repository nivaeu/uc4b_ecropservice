/*
* Copyright 2020 Wageningen Environmental Research
* For licensing information read the included LICENSE.txt file.
* Unless required by applicable law or agreed to in writing, this software
* is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF
* ANY KIND, either express or implied.
 */
package niva.nl.rvo.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import niva.nl.rvo.db.Db;
import niva.nl.rvo.model.PhysicalSpecifiedGeographicalFeature;
import niva.nl.rvo.model.Properties;
import org.postgresql.util.PGobject;


/**
 * Return as applied map.
 *
 * @author Rivie003
 */
@Path("/aam")
@Produces(MediaType.APPLICATION_JSON)
public class AAM {
    public AAM() {
        super();
    }

    /**
     * Retrieve all ID's as an JSON-array. Not feasible when many rows are present [temporary solution].
     * When no rows are present an empty array is returned.
     * @param uriInfo
     * @param username
     * @return 
     * @throws java.lang.Exception
     */
    @GET
    @Path("/")
    public Response getAAM(@Context UriInfo uriInfo, @HeaderParam("username") String username) throws Exception {
        String result;
        Db db = new Db();
        String komma = "";
        
        System.out.println("getAAM ID " + uriInfo.getPath());
            
        ResultSet resultSet = db.executeSelect("SELECT ID FROM niva.AAM_ID ORDER BY ID",  new Object[0]);
        try {
            result = "{\""+uriInfo.getPath().split("/")[1]+"\":[";
            
            while (resultSet.next()) {
                result = result.concat(komma+resultSet.getInt(1));
                komma = ",";
            }

            result = result.concat("]}");
        } catch (Exception e) {
            Logger.getLogger(AAM.class.getName()).log(Level.SEVERE, "Exception", e);
            return Response.status(Status.BAD_REQUEST).build();
        }
        finally {
            if (resultSet.getStatement() != null)
                resultSet.getStatement().close();
            resultSet.close();
        }
        System.out.println("return getAAM ID " + result);
        return Response.status(Status.OK).entity(result).build();
    }
    @POST
    @Path("")
    public Response getAAMPost(@Context UriInfo uriInfo, @HeaderParam("username") String username) throws Exception {
        return getAAM(uriInfo, username);
    }
    
    /**
     * Retrieve all as applied map features for a requested id and return them as a feature collection.
     * @param uriInfo (not used)
     * @param username (not used)
     * @param id    the requested id (= crop report ID)
     * @return  response with feature collection (OK) or null (NOT_FOUND)
     * @throws java.lang.Exception
     */
    @GET
    @Path("/{id:[0-9]*}")
    public Response getAAMID(@Context UriInfo uriInfo, @HeaderParam("username") String username, @PathParam("id") long id, @QueryParam("page") int page) throws Exception {
        String result;
        //java.util.Properties props = parametersToProperties(uriInfo);
        //int offset = 0;
        //if (props.containsKey("page"))
        //    offset = Integer.getInteger(props.getProperty("page"))*500;
        
        Db db = new Db();
        Object[] parameters = new Object[1];
        parameters[0] = id;
        String rowkomma = "";
        ObjectMapper om = new ObjectMapper();
        ObjectWriter ow = new ObjectMapper().writer();
        
        System.out.println("getAAM/ID recursive " + id + " " + uriInfo.getPath() + " offset " + page*500);
            
        ResultSet resultSet = db.executeSelect(String.format("SELECT * FROM niva.asappliedmap where ID = ? limit 500 offset %d", page*500),  parameters);
        
        // get the column names
        ResultSetMetaData md = resultSet.getMetaData();
        int nColumns = md.getColumnCount();
        String[] columnNames = new String[nColumns];
        for (int i = 1; i <= nColumns; i++) {
            columnNames[i - 1] = md.getColumnLabel(i);
        }
        
        try {
            if (resultSet.first()) {
                result = "{\"type\": \"FeatureCollection\", \"features\": [";

                do {
                    PhysicalSpecifiedGeographicalFeature feature = (PhysicalSpecifiedGeographicalFeature)om.readValue(((PGobject)resultSet.getObject("feature")).getValue(), PhysicalSpecifiedGeographicalFeature.class);
                    
                    // fix geojson (from SEGES)
                    if (feature.getProperties() == null)
                        feature.setProperties(new Properties());
                    
                    if (!feature.getType().equals("Feature"))               // always
                        feature.setType("Feature");
                    
                    if (!feature.getGeometry().getType().equals("Polygon")) // should be for this version
                        feature.getGeometry().setType("Polygon");
                    // end fix
                    
                    for (int i = 0; i < nColumns; i++) {
                        if (!"feature".equals(columnNames[i])) {
                            if ("id".equals(columnNames[i]))
                                feature.getProperties().setAdditionalProperty("id", resultSet.getInt("id"));
                            else if ("creationdatetime".equals(columnNames[i]))
                                feature.getProperties().setAdditionalProperty(columnNames[i], resultSet.getTimestamp(columnNames[i]).toString());
                            else 
                                feature.getProperties().setAdditionalProperty(columnNames[i], (String)resultSet.getObject(columnNames[i]));
                        }
                    }

                    result = result.concat(rowkomma+ow.writeValueAsString(feature));
                    rowkomma = ",";
                } while (resultSet.next());

                result = result.concat("]}");
            }
            else {
                return Response.status(Status.NOT_FOUND).build();
            }
        } catch (Exception e) {
            Logger.getLogger(AAM.class.getName()).log(Level.SEVERE, "Exception on requested id: "+id, e);
            return Response.status(Status.BAD_REQUEST).build();
        }
        finally {
            if (resultSet.getStatement() != null) {
                // double close on connection
                resultSet.getStatement().close();
                resultSet.close();
            }
        }
        System.out.println("return getAAM/ID recursive " + id + " " + result.substring(0, 50));
        return Response.status(Status.OK).entity(result).build();
    }
    @POST
    @Path("/{id:[0-9]*}")
    public Response getAAMIDPost(@Context UriInfo uriInfo, @HeaderParam("username") String username, @PathParam("id") long id, @QueryParam("page") int page) throws Exception {
        return getAAMID(uriInfo, username, id, page);
    }
    
    /**
     * Convert the supplied query and path parameters to a property list. In
     * some cases parametervalue is LIST<String>.
     *
     * @param uriInfo
     * @return
     */
    protected java.util.Properties parametersToProperties(UriInfo uriInfo) {
        java.util.Properties props = new java.util.Properties();
        Iterator<Map.Entry<String, List<String>>> iterator = uriInfo.getQueryParameters().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<String>> element = iterator.next();
            Object value = element.getValue();
            if (value instanceof java.lang.String) {
                props.put(element.getKey().toLowerCase(), value);  // getValue always a list for query parameters.
            } else if (value instanceof java.util.LinkedList) {
                LinkedList l = (LinkedList) value;
                props.put(element.getKey().toLowerCase(), l.get(0));
            }
        }

        iterator = uriInfo.getPathParameters().entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, List<String>> element = iterator.next();
            Object value = element.getValue();
            if (value instanceof java.lang.String) {
                props.put(element.getKey().toLowerCase(), value);  // getValue always a list for path parameters.
            } else if (value instanceof java.util.ArrayList) {
                ArrayList l = (ArrayList) value;
                props.put(element.getKey().toLowerCase(), l.get(0));
            }
        }
        
        return props;
    }    
}