/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

/*
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
*/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.json.simple.JSONObject;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "Name",
    "TypeCode",
    "Description",
    "ReferenceTypeCode",
    "PhysicalSpecifiedGeographicalFeature"
})
public class SpecifiedReferencedLocation {

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("TypeCode")
    private String typeCode;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("ReferenceTypeCode")
    private String referenceTypeCode;
    @JsonProperty("PhysicalSpecifiedGeographicalFeature")
    private PhysicalSpecifiedGeographicalFeature physicalSpecifiedGeographicalFeature;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("TypeCode")
    public String getTypeCode() {
        return typeCode;
    }

    @JsonProperty("TypeCode")
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("Description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("ReferenceTypeCode")
    public String getReferenceTypeCode() {
        return referenceTypeCode;
    }

    @JsonProperty("ReferenceTypeCode")
    public void setReferenceTypeCode(String referenceTypeCode) {
        this.referenceTypeCode = referenceTypeCode;
    }

    @JsonProperty("PhysicalSpecifiedGeographicalFeature")
    public PhysicalSpecifiedGeographicalFeature getPhysicalSpecifiedGeographicalFeature() {
        return physicalSpecifiedGeographicalFeature;
    }

    @JsonProperty("PhysicalSpecifiedGeographicalFeature")
    public void setPhysicalSpecifiedGeographicalFeature(PhysicalSpecifiedGeographicalFeature physicalSpecifiedGeographicalFeature) {
        this.physicalSpecifiedGeographicalFeature = physicalSpecifiedGeographicalFeature;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
    private String getGeoJSON() {
        JSONObject result = new JSONObject();
        result.put("type", this.getPhysicalSpecifiedGeographicalFeature().getType());

        JSONObject propertiesjsonobject = new JSONObject();
        propertiesjsonobject.put("id",                  this.getID());
        propertiesjsonobject.put("name",                this.getName());
        propertiesjsonobject.put("typeCode",            this.getTypeCode());
        propertiesjsonobject.put("description",         this.getDescription());
        propertiesjsonobject.put("referenceTypeCode",   this.getReferenceTypeCode());
        result.put("properties", propertiesjsonobject);

        JSONObject geometryjsonobject = new JSONObject();
        geometryjsonobject.put("type",          this.getPhysicalSpecifiedGeographicalFeature().getGeometry().getType());
        geometryjsonobject.put("coordinates",   this.getPhysicalSpecifiedGeographicalFeature().getGeometry().getCoordinates());
        result.put("geometry", geometryjsonobject);

        return result.toJSONString();
    }
}
