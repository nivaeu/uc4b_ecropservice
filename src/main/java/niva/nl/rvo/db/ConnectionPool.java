/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.db;

import java.sql.Connection;
import org.postgresql.ds.PGPoolingDataSource;
import niva.nl.rvo.core.NivaECropProperties;

/**
 *
 * @author rande001, rivie003
 */
public class ConnectionPool {

    private static PGPoolingDataSource pool;

    static {
        try {
            pool = new PGPoolingDataSource();
            pool.setApplicationName("NIVA UC4b Backend");
            pool.setDatabaseName(NivaECropProperties.getStringProperty("db.database"));
            pool.setUser(NivaECropProperties.getStringProperty("db.username"));
            pool.setPassword(NivaECropProperties.getStringProperty("db.password"));
            pool.setPortNumber(NivaECropProperties.getIntegerProperty("db.port"));
            pool.setServerName(NivaECropProperties.getStringProperty("db.host"));
            pool.setMaxConnections(NivaECropProperties.getIntegerProperty("db.max_connections"));
        }
        catch (Exception e) {
            System.out.println("ConnectionPool static ".concat(e.getMessage()));
        }
    }

    public static Connection getConnection() throws Exception {
        try {
            synchronized (pool) {
                return pool.getConnection();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw e;
        }
    }

}
