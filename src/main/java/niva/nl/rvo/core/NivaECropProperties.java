/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.core;

import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author rande001, rivie003
 */
public class NivaECropProperties {

    private static java.util.Properties props;

    public static double getDoubleProperty(String propertyName) throws Exception {
        try {
            return Double.parseDouble(getProperty(propertyName));
        }
        catch (Exception e) {
            throw new RuntimeException(String.format("NivaECropProperties.getDoubleProperty : Unable to convert %s to double, is a %s",getProperty(propertyName).toString(),getProperty(propertyName).getClass().getCanonicalName()));
        }
    }

    public static Integer getIntegerProperty(String propertyName) throws Exception {
        try {
            return Integer.parseInt(getProperty(propertyName));
        }
        catch (Exception e) {
            throw new RuntimeException(String.format("NivaECropProperties.getIntegerProperty : Unable to convert %s to String",getProperty(propertyName).toString()));
        }
    }
    
    public static String getStringProperty(String propertyName) throws Exception {
        try {
            Object o= getProperty(propertyName);
            return o.toString();
        }
        catch (Exception e) {
            throw new RuntimeException(String.format("NivaECropProperties.getStringProperty : Unable to convert %s to String",getProperty(propertyName).toString()));
        }
    }

    private static String getProperty(String propertyName) throws Exception {
        if (props == null) {
            loadProps();
        }
        String s = props.getProperty(propertyName.toLowerCase());
        if (s == null) {
            throw new RuntimeException(String.format("Property %s not found in properties file", propertyName.toLowerCase()));
        }
        return s;
    }

    private static void loadProps() throws Exception {
        try {
            String streamName = "niva-ecrop.properties";
            InputStream in = NivaECropProperties.class.getClassLoader().getResourceAsStream(streamName);
            if (in == null) {
                throw new RuntimeException(String.format("Unable to open stream for properties file %s", streamName));
            }
            props = new Properties();
            props.load(in);
        } catch (Exception e) {
            props = null;
            throw new RuntimeException(e.getMessage());
        }
    }
}
