/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.ArrayList;
import niva.nl.rvo.core.UniformTimestamp;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ReturnCode",       // 0 = No errors, 1 = Debug (?), 2 = Info Messages, 5 = Warnings, 9 = errors encountered
    "ReturnMessage",    // 'Received Ok', 'Validated Ok' (for ReturnCode 0), 'Warnings', 'Errors encountered'
    "MessageCount",
    "TimeStamp",
    "Messages"
})
public class ECROPReturnMessage {
    /**
     *
     * @param returnCode
     * @param returnMessage
     * @param messageCount
     * @param timeStamp
     * @param messages
     */
    @JsonCreator
    public ECROPReturnMessage(@JsonProperty(value = "ReturnCode", required = true) int returnCode, 
                              @JsonProperty(value = "ReturnMessage", required = true) String returnMessage, 
                              @JsonProperty(value = "MessageCount", required = true) int messageCount, 
                              @JsonProperty(value = "TimeStamp", required = true) String timeStamp,
                              @JsonProperty("Messages") ArrayList<Message> messages) {
        this.returnCode = returnCode;
        this.returnMessage = returnMessage;
        this.messageCount = messageCount;
        this.timeStamp = timeStamp;
        this.messages = messages;
    }

    /**
     *
     * @param returnCode
     * @param returnMessage
     * @param messageCount
     */
    public ECROPReturnMessage(int returnCode, String returnMessage) {
        this.returnCode = returnCode;
        this.returnMessage = returnMessage;
        this.messageCount = 0;
        this.timeStamp = UniformTimestamp.getUniformTimestamp();
        this.messages = null;
    }

    public ECROPReturnMessage(int returnCode, String returnMessage, ArrayList<Message> messages) {
        this.returnCode = returnCode;
        this.returnMessage = returnMessage;
        this.messageCount = messages.size();
        this.timeStamp = UniformTimestamp.getUniformTimestamp();
        this.messages = messages;
    }

    @JsonProperty("ReturnCode")
    private Integer returnCode;
    
    @JsonProperty("ReturnMessage")
    private String returnMessage;
    
    @JsonProperty("MessageCount")
    private Integer messageCount;

    @JsonProperty("TimeStamp")
    private String timeStamp;
    
    @JsonProperty("Messages")
    private ArrayList<Message> messages = null;
    
    
    @JsonProperty("ReturnMessage")
    public String getReturnMessage() {
        return returnMessage;
    }

    @JsonProperty("ReturnMessage")
    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    @JsonProperty("ReturnCode")
    public Integer getReturnCode() {
        return returnCode;
    }

    @JsonProperty("ReturnCode")
    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    @JsonProperty("TimeStamp")
    public String getTimeStamp() {
        return timeStamp;
    }

    @JsonProperty("TimeStamp")
    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    @JsonProperty("MessageCount")
    public Integer getMessageCount() {
        return messageCount;
    }

    @JsonProperty("MessageCount")
    public void setMessageCount(Integer messageCount) {
        this.messageCount = messageCount;
    }
    
    @JsonProperty("Messages")
    public ArrayList<Message> getMessages() {
        return messages;
    }

    @JsonProperty("Messages")
    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
        this.messageCount = this.messages.size();
    }
}