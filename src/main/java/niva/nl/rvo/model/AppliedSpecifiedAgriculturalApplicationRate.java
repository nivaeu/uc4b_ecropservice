/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "AppliedQuantity",
    "AppliedQuantityUnit",
    "AppliedReferencedLocation"
})
public class AppliedSpecifiedAgriculturalApplicationRate {

    @JsonProperty("AppliedQuantity")
    private Integer appliedQuantity;
    @JsonProperty("AppliedQuantityUnit")
    private String appliedQuantityUnit;
    @JsonProperty("AppliedReferencedLocation")
    private List<AppliedReferencedLocation> appliedReferencedLocation = null;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("AppliedQuantity")
    public Integer getAppliedQuantity() {
        return appliedQuantity;
    }

    @JsonProperty("AppliedQuantity")
    public void setAppliedQuantity(Integer appliedQuantity) {
        this.appliedQuantity = appliedQuantity;
    }

    @JsonProperty("AppliedQuantityUnit")
    public String getAppliedQuantityUnit() {
        return appliedQuantityUnit;
    }

    @JsonProperty("AppliedQuantityUnit")
    public void setAppliedQuantityUnit(String appliedQuantityUnit) {
        this.appliedQuantityUnit = appliedQuantityUnit;
    }

    @JsonProperty("AppliedReferencedLocation")
    public List<AppliedReferencedLocation> getAppliedReferencedLocation() {
        return appliedReferencedLocation;
    }

    @JsonProperty("AppliedReferencedLocation")
    public void setAppliedReferencedLocation(List<AppliedReferencedLocation> appliedReferencedLocation) {
        this.appliedReferencedLocation = appliedReferencedLocation;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
