/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "Name",
    "Description",
    "ClassificationCode",
    "AgriculturalProductionUnit"
})
public class AgriculturalProducerParty {
     /**
     * @param iD
     * @param name
     * @param description
     * @param classificationCode
     * @param agriculturalProductionUnit
     */
    // http://tutorials.jenkov.com/java-json/jackson-annotations.html
    @JsonCreator
    public AgriculturalProducerParty(@JsonProperty(value = "ID", required = true) String iD, 
                                    @JsonProperty(value = "Name", required = true) String name, 
                                    @JsonProperty("Description") String description, 
                                    @JsonProperty("ClassificationCode") String classificationCode, 
                                    @JsonProperty("AgriculturalProductionUnit") AgriculturalProductionUnit agriculturalProductionUnit) {
        this.iD = iD;
        this.name = name;
        this.description = description;
        this.classificationCode = classificationCode;
        this.agriculturalProductionUnit = agriculturalProductionUnit;
    }

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("ClassificationCode")
    private String classificationCode;
    @JsonProperty("AgriculturalProductionUnit")
    private AgriculturalProductionUnit agriculturalProductionUnit;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("Description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("ClassificationCode")
    public String getClassificationCode() {
        return classificationCode;
    }

    @JsonProperty("ClassificationCode")
    public void setClassificationCode(String classificationCode) {
        this.classificationCode = classificationCode;
    }

    @JsonProperty("AgriculturalProductionUnit")
    public AgriculturalProductionUnit getAgriculturalProductionUnit() {
        return agriculturalProductionUnit;
    }

    @JsonProperty("AgriculturalProductionUnit")
    public void setAgriculturalProductionUnit(AgriculturalProductionUnit agriculturalProductionUnit) {
        this.agriculturalProductionUnit = agriculturalProductionUnit;
    }

    /*@JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
