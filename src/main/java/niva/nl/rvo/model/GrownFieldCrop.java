/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ClassificationCode",
    "SpecifiedFieldCropMixtureConstituent",
    "ApplicableCropProductionAgriculturalProcess"
})
public class GrownFieldCrop {

    @JsonProperty("ClassificationCode")
    private String classificationCode;
    @JsonProperty("SpecifiedFieldCropMixtureConstituent")
    private List<SpecifiedFieldCropMixtureConstituent> specifiedFieldCropMixtureConstituent = null;
    @JsonProperty("ApplicableCropProductionAgriculturalProcess")
    private List<ApplicableCropProductionAgriculturalProcess> applicableCropProductionAgriculturalProcess = null;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ClassificationCode")
    public String getClassificationCode() {
        return classificationCode;
    }

    @JsonProperty("ClassificationCode")
    public void setClassificationCode(String classificationCode) {
        this.classificationCode = classificationCode;
    }

    @JsonProperty("SpecifiedFieldCropMixtureConstituent")
    public List<SpecifiedFieldCropMixtureConstituent> getSpecifiedFieldCropMixtureConstituent() {
        return specifiedFieldCropMixtureConstituent;
    }

    @JsonProperty("SpecifiedFieldCropMixtureConstituent")
    public void setSpecifiedFieldCropMixtureConstituent(List<SpecifiedFieldCropMixtureConstituent> specifiedFieldCropMixtureConstituent) {
        this.specifiedFieldCropMixtureConstituent = specifiedFieldCropMixtureConstituent;
    }

    @JsonProperty("ApplicableCropProductionAgriculturalProcess")
    public List<ApplicableCropProductionAgriculturalProcess> getApplicableCropProductionAgriculturalProcess() {
        return applicableCropProductionAgriculturalProcess;
    }

    @JsonProperty("ApplicableCropProductionAgriculturalProcess")
    public void setApplicableCropProductionAgriculturalProcess(List<ApplicableCropProductionAgriculturalProcess> applicableCropProductionAgriculturalProcess) {
        this.applicableCropProductionAgriculturalProcess = applicableCropProductionAgriculturalProcess;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
