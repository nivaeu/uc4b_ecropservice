/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author rande001, rivie003
 */
public class Db {

    PreparedStatement insertCropReport = null;
    //PreparedStatement updateCropReport = null;
    Connection con = null;

    public Connection getConnection() throws Exception {
        if (con == null) {
            System.out.println("Connection retrieved ");
            con = ConnectionPool.getConnection();
            con.setAutoCommit(true);
        }
        return con;
    }

   /**
     * execute select statement and return result set.
     *
     * @param statement
     * @param parameters
     * @return
     */
    public ResultSet executeSelect(String statement, Object[] parameters) {
        PreparedStatement ps = null;
        try {
            ps = getConnection().prepareStatement(statement, ResultSet.FETCH_FORWARD, ResultSet.CONCUR_READ_ONLY);
            for (int i = 0; i < parameters.length; i++) {
                ps.setObject(i + 1, parameters[i]);
            }
            ps.setFetchSize(500);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (Exception e) {
            try {
                ps.close();
            } catch (Exception ww) {;}
            
            // failing connection will throw exception -> leads to 500
            throw new RuntimeException(statement.concat(" caused ").concat(e.getMessage()));
        }
        finally{
            close();            
        }
    }

    /**
     * Save a crop report. Caller takes care of connection and commit/rollback and closes connection.
     *
     * @param con
     * @param issuer
     * @param message
     * @param returnMessage
     * @throws java.sql.SQLException
     */
    public void saveCropReport(Connection con, String issuer, String message, String returnMessage) throws SQLException {

        if (insertCropReport == null) {
            // niva.CropReport (CreationDateTime, Issuer, ECropReportMessage, ECropReturnMessage)
            insertCropReport = con.prepareStatement("insert into niva.CropReport (CreationDateTime, Issuer, ECropReportMessage, ECropReturnMessage) values (CURRENT_TIMESTAMP, ?,?::JSONB,?::JSONB)");
        }

        insertCropReport.setObject(1, issuer);
        insertCropReport.setObject(2, message);
        insertCropReport.setObject(3, returnMessage);
        insertCropReport.execute();     
    }

    public void close() {
        try {
            if (con == null)
                System.out.println("No connection to close");
            else {
                con.close();
                System.out.println("Connection closed");
            }
        } catch (Exception e) {
            System.out.println("close: "+e.getLocalizedMessage()); // ignore.
        }
    }
}
