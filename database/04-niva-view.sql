/* 
 * Create view for the niva database, niva schema on test
 * Usage: psql -U niva -d niva -h scomp1271.wurnet.nl -d niva -f 04-niva-view.sql 
 * https://lluad.com/blog/2013/08/27/postgresql-and-iso-8601-timestamps/
 * https://phili.pe/posts/timestamps-and-time-zones-in-postgresql/
 */

BEGIN;

/* AAM ID's */
DROP VIEW IF EXISTS niva.AAM_ID CASCADE;

CREATE OR REPLACE VIEW niva.AAM_ID AS
SELECT r.id, r.issuer, to_char(r.CreationDateTime at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') as iso8601stamp
FROM cropreport r
WHERE r.ECropReturnMessage is not null 
and (r.ECropReturnMessage->'ECROPReturnMessage'->> 'ReturnCode') = '0'
;

/* AAM old */
DROP VIEW IF EXISTS niva.AppliedMap CASCADE;

CREATE VIEW niva.AppliedMap AS SELECT 
ol.ID,
--ol.CreationDateTime,
to_char(ol.CreationDateTime at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') as iso8601stamp,
ol.Issuer,
(SELECT string_agg(ts.*, ', ') from niva.cropreport il,
	cast(
		(
			jsonb_array_elements(
				jsonb_array_elements(
					jsonb_array_elements(
						jsonb_array_elements(
							jsonb_array_elements(
								ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'
							)->'GrownFieldCrop'
						)->'ApplicableCropProductionAgriculturalProcess'
					)->'AppliedSpecifiedAgriculturalApplication'
				)->'SpecifiedProductBatch'
			)->>'AppliedTreatment'
		) as text
	) as ts
where il.id = ol.id) as appliedtreatments,
(SELECT string_agg(pn.*, ', ') from niva.cropreport il,
	cast(
		(
			jsonb_array_elements(
				jsonb_array_elements(
					jsonb_array_elements(
						jsonb_array_elements(
							jsonb_array_elements(
								ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'
							)->'GrownFieldCrop'
						)->'ApplicableCropProductionAgriculturalProcess'
					)->'AppliedSpecifiedAgriculturalApplication'
				)->'SpecifiedProductBatch'
			)->>'ProductName'
		) as text
	) as pn
where il.id = ol.id) as productnames,
(SELECT string_agg(bn.*, ', ') from niva.cropreport il,
	cast(
		(
			(
				jsonb_array_elements(
					jsonb_array_elements(
						jsonb_array_elements(
							ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot'
						)->'GrownFieldCrop'
					)->'SpecifiedFieldCropMixtureConstituent'
				)->'SpecifiedBotanicalCrop'
			)->>'BotanicalName'
		) as text
	) as bn
where il.id = ol.id) as botanicalnames,
jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot')->'GrownFieldCrop')->'ApplicableCropProductionAgriculturalProcess')->'AppliedSpecifiedAgriculturalApplication')->'AppliedSpecifiedAgriculturalApplicationRate')->'AppliedReferencedLocation')->'PhysicalSpecifiedGeographicalFeature' as feature,
(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot')->'GrownFieldCrop')->'ApplicableCropProductionAgriculturalProcess')->>'ActualStartDateTime') as actualstartdatetime,
(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot')->'GrownFieldCrop')->'ApplicableCropProductionAgriculturalProcess')->'AppliedSpecifiedAgriculturalApplication')->'AppliedSpecifiedAgriculturalApplicationRate')->>'AppliedQuantity') as quantity,
(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot')->'GrownFieldCrop')->'ApplicableCropProductionAgriculturalProcess')->'AppliedSpecifiedAgriculturalApplication')->'AppliedSpecifiedAgriculturalApplicationRate')->>'AppliedQuantityUnit') as quantityunit

from niva.cropreport ol

where ECropReturnMessage is not null 
and (ECropReturnMessage->'ECROPReturnMessage'->> 'ReturnCode') = '0';

/* NEW: recursive */
Drop View niva.AsAppliedMap;
create view asappliedmap as
WITH RECURSIVE json_recursive as ( SELECT
    a.id,
    b.k,
    b.v,
    b.json_type,
    case when b.json_type = 'object' and not (b.v->>'AppliedQuantity') is null then b.v->>'AppliedQuantity' else a.Applied_Quantity end Applied_Quantity,
    case when b.json_type = 'object' and not (b.v->>'AppliedQuantityUnit') is null then b.v->>'AppliedQuantityUnit' else a.Applied_Quantity_Unit end Applied_Quantity_Unit,
    case when b.json_type = 'object' and not (b.v->>'ActualStartDateTime') is null then b.v->>'ActualStartDateTime' else a.Actual_Start_DateTime end Actual_Start_DateTime
  FROM ( SELECT
        id,
        ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot' v,
        case left((ECropReportMessage->'ECROPReportMessage'->'AgriculturalProducerParty'->'AgriculturalProductionUnit'->'SpecifiedCropPlot')::text,1)
          when '[' then 'array'
          when '{' then 'object'
          else 'scalar'
        end json_type, 										--because choice of json accessor function depends on this, and for some reason postgres has no built in function to get this value
        ECropReportMessage->>'dummy' Applied_Quantity,
        ECropReportMessage->>'dummy' Applied_Quantity_Unit,
        ECropReportMessage->>'dummy' Actual_Start_DateTime
      FROM CropReport
	  WHERE ECropReturnMessage is not null 
	  and (ECropReturnMessage->'ECROPReturnMessage'->> 'ReturnCode') = '0'
    ) a
    CROSS JOIN LATERAL ( SELECT
        b.k,
        b.v,
        case left(b.v::text,1)
          when '[' then 'array'
          when '{' then 'object'
          else 'scalar'
        end json_type
      FROM jsonb_each(case json_type when 'object' then a.v else null end ) b(k,v) 						--get key value pairs for individual elements if we are dealing with standard object
     UNION ALL SELECT
        null::text k,
        c.v,
        case left(c.v::text,1)
          when '[' then 'array'
          when '{' then 'object'
          else 'scalar'
        end json_type
      FROM jsonb_array_elements(case json_type when 'array' then a.v else null end) c(v) 									--if we have an array, just get the elements and use parent key
    ) b
UNION ALL SELECT																													--recursive term
    a.id,
    b.k,
    b.v,
    b.json_type,
    case when b.json_type = 'object' and not (b.v->>'AppliedQuantity') is null then b.v->>'AppliedQuantity' else a.Applied_Quantity end Applied_Quantity,
    case when b.json_type = 'object' and not (b.v->>'AppliedQuantityUnit') is null then b.v->>'AppliedQuantityUnit' else a.Applied_Quantity_Unit end Applied_Quantity_Unit,
    case when b.json_type = 'object' and not (b.v->>'ActualStartDateTime') is null then b.v->>'ActualStartDateTime' else a.Actual_Start_DateTime end Actual_Start_DateTime
  FROM json_recursive a
    CROSS JOIN LATERAL ( SELECT
        b.k,
        b.v,
        case left(b.v::text,1)
          when '[' then 'array'
          when '{' then 'object'
          else 'scalar'
        end json_type
      FROM jsonb_each(case json_type when 'object' then a.v else null end ) b(k,v)
     UNION ALL SELECT
        a.k,
        c.v,
        case left(c.v::text,1)
          when '[' then 'array'
          when '{' then 'object'
          else 'scalar'
        end json_type
      FROM jsonb_array_elements(case json_type when 'array' then a.v else null end) c(v)
    ) b
) SELECT r.id, r.issuer, r.iso8601stamp,
 v as feature, Applied_Quantity || ' ' || Applied_Quantity_Unit as AppliedQuantity, Actual_Start_DateTime as ActualStartDateTime
, ( SELECT string_agg(pn.pn, ', '::text) AS string_agg FROM cropreport il,
    LATERAL CAST(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(jsonb_array_elements((((il.ecropreportmessage -> 'ECROPReportMessage'::text) -> 'AgriculturalProducerParty'::text) -> 'AgriculturalProductionUnit'::text) -> 'SpecifiedCropPlot'::text) -> 'GrownFieldCrop'::text) -> 'ApplicableCropProductionAgriculturalProcess'::text) -> 'AppliedSpecifiedAgriculturalApplication'::text) -> 'SpecifiedProductBatch'::text) ->> 'ProductName'::text AS text) pn(pn)
    WHERE il.id = r.id) AS productnames
, ( SELECT string_agg(bc.bc, ', '::text) AS string_agg FROM cropreport il,
    LATERAL CAST((jsonb_array_elements(jsonb_array_elements(jsonb_array_elements((((il.ecropreportmessage -> 'ECROPReportMessage'::text) -> 'AgriculturalProducerParty'::text) -> 'AgriculturalProductionUnit'::text) -> 'SpecifiedCropPlot'::text) -> 'GrownFieldCrop'::text) -> 'SpecifiedFieldCropMixtureConstituent'::text) -> 'SpecifiedBotanicalCrop'::text) ->> 'BotanicalSpeciesCode'::text AS text) bc(bc)
    WHERE il.id = r.id) AS botanicalspeciescods
FROM json_recursive jf, aam_id r
WHERE r.id = jf.id
and jf.k = 'PhysicalSpecifiedGeographicalFeature' 
and jf.json_type = 'object' 
and jf.Applied_Quantity is not null
;

END;