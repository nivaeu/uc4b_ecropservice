/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ID",
    "TypeCode",
    "Name",
    "SpecifiedCropPlot"
})
public class AgriculturalProductionUnit {

    @JsonProperty("ID")
    private String iD;
    @JsonProperty("TypeCode")
    private String typeCode;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("SpecifiedCropPlot")
    private List<SpecifiedCropPlot> specifiedCropPlot = null;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ID")
    public String getID() {
        return iD;
    }

    @JsonProperty("ID")
    public void setID(String iD) {
        this.iD = iD;
    }

    @JsonProperty("TypeCode")
    public String getTypeCode() {
        return typeCode;
    }

    @JsonProperty("TypeCode")
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("SpecifiedCropPlot")
    public List<SpecifiedCropPlot> getSpecifiedCropPlot() {
        return specifiedCropPlot;
    }

    @JsonProperty("SpecifiedCropPlot")
    public void setSpecifiedCropPlot(List<SpecifiedCropPlot> specifiedCropPlot) {
        this.specifiedCropPlot = specifiedCropPlot;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
