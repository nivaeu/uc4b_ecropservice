/* 
 * Create niva schema, extension postgis
 * Usage: psql -U postgres -d niva -h scomp1271.wurnet.nl -f 01-niva-schema.sql 
 */
 
DROP SCHEMA IF EXISTS niva;

CREATE SCHEMA niva;

CREATE EXTENSION postgis schema niva;

ALTER SCHEMA niva OWNER TO niva;

GRANT ALL PRIVILEGES ON SCHEMA niva to postgres;
GRANT ALL PRIVILEGES ON SCHEMA niva to niva;

/* or niva.?
GRANT ALL PRIVILEGES ON TABLE public.geometry_columns to niva;

GRANT ALL PRIVILEGES ON TABLE public.geography_columns to niva;

GRANT ALL PRIVILEGES ON TABLE public.spatial_ref_sys to niva;
*/