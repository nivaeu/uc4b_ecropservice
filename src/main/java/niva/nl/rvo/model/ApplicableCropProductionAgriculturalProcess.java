/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.model;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TypeCode",
    "SubordinateTypeCode",
    "ActualStartDateTime",
    "ActualEndDateTime",
    "AppliedSpecifiedAgriculturalApplication"
})
public class ApplicableCropProductionAgriculturalProcess {

    @JsonProperty("TypeCode")
    private String typeCode;
    @JsonProperty("SubordinateTypeCode")
    private String subordinateTypeCode;
    @JsonProperty("ActualStartDateTime")
    private String actualStartDateTime;
    @JsonProperty("ActualEndDateTime")
    private String actualEndDateTime;
    @JsonProperty("AppliedSpecifiedAgriculturalApplication")
    private List<AppliedSpecifiedAgriculturalApplication> appliedSpecifiedAgriculturalApplication = null;
    //@JsonIgnore
    //private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("TypeCode")
    public String getTypeCode() {
        return typeCode;
    }

    @JsonProperty("TypeCode")
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    @JsonProperty("SubordinateTypeCode")
    public String getSubordinateTypeCode() {
        return subordinateTypeCode;
    }

    @JsonProperty("SubordinateTypeCode")
    public void setSubordinateTypeCode(String subordinateTypeCode) {
        this.subordinateTypeCode = subordinateTypeCode;
    }

    @JsonProperty("ActualStartDateTime")
    public String getActualStartDateTime() {
        return actualStartDateTime;
    }

    @JsonProperty("ActualStartDateTime")
    public void setActualStartDateTime(String actualStartDateTime) {
        this.actualStartDateTime = actualStartDateTime;
    }

    @JsonProperty("ActualEndDateTime")
    public String getActualEndDateTime() {
        return actualEndDateTime;
    }

    @JsonProperty("ActualEndDateTime")
    public void setActualEndDateTime(String actualEndDateTime) {
        this.actualEndDateTime = actualEndDateTime;
    }

    @JsonProperty("AppliedSpecifiedAgriculturalApplication")
    public List<AppliedSpecifiedAgriculturalApplication> getAppliedSpecifiedAgriculturalApplication() {
        return appliedSpecifiedAgriculturalApplication;
    }

    @JsonProperty("AppliedSpecifiedAgriculturalApplication")
    public void setAppliedSpecifiedAgriculturalApplication(List<AppliedSpecifiedAgriculturalApplication> appliedSpecifiedAgriculturalApplication) {
        this.appliedSpecifiedAgriculturalApplication = appliedSpecifiedAgriculturalApplication;
    }
/*
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
*/
}
