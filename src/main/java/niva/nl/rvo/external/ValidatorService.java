/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.external;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
//import com.fasterxml.jackson.core.JsonParseException
import niva.nl.rvo.core.NivaECropProperties;
import niva.nl.rvo.core.UniformTimestamp;

/**
 * This class implements the client that connects to the (validator) service.
 *
 * @author rande001, rivie003
 */
public class ValidatorService {
    /**
     * Call validator service. Just a Stub...
     *
     * @param data
     * @return
     */
    public String process(String data) {
        String validatorResult;
        try {
            System.out.println("validator in: " + data);
            
            validatorResult = callService(data);
            
            System.out.println("validator out: " + validatorResult);

            //
            // process the results
            JSONParser parser = new JSONParser();
            JSONObject result = (JSONObject)parser.parse(validatorResult);
        } catch (Exception e) {
            throw new RuntimeException("ValidatorService.process " + e.getLocalizedMessage());
        }
        return validatorResult;
    }

    /**
     * Call the internal PA service.
     *
     * @param inputJson
     * @return
     */
    private String callService(String inputJson) {
        String urlString = "";
        StringBuilder result = new StringBuilder();
        try {
            urlString = NivaECropProperties.getStringProperty("validatorservice.url");
            if (urlString.isEmpty()) {
                return "{\"ECROPReturnMessage\":" +
" {\"ReturnCode\": 0," +
"  \"ReturnMessage\": \"Ok (no validator url)\"," +
"  \"MessageCount\": 0," +
"  \"TimeStamp\": \"" + UniformTimestamp.getUniformTimestamp() + "\"" +
" }" +
"}";
            }
            //https://stackoverflow.com/questions/21404252/post-request-send-json-data-java-httpurlconnection
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(5000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);   // to allow POST body stream
            connection.setDoInput(true);    // always true
            
            OutputStream os = connection.getOutputStream();
            os.write(inputJson.getBytes("UTF-8"));
            os.close();

            InputStream is = (InputStream)connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            is.close();
            System.out.println("result " + result.toString());
            return result.toString();
        } catch (Exception e) {
            System.out.println("------------- START ---------------");
            //System.out.println("Sending json " + inputJson);
            System.out.println("result " + result.toString());
            System.out.println("URL " + urlString);
            System.out.println("Message ".concat(e.getLocalizedMessage()));
            throw new RuntimeException("validator: ".concat(e.getLocalizedMessage()));
        }
    }
}
