/*
 *
 * Copyright (c) WENR 2019 -- 2021.
 * This file belongs to subproject UC4B of project NIVA (www.niva4cap.eu)
 * All rights reserved
 *
 * Project and code is made available under the EU-PL v 1.2 license.
 *
*/

package niva.nl.rvo.internal;
