/*
* Copyright 2020 Wageningen Environmental Research
*
* For licensing information read the included LICENSE.txt file.
*
* Unless required by applicable law or agreed to in writing, this software
* is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF
* ANY KIND, either express or implied.
*/
package niva.nl.rvo.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import niva.nl.rvo.db.Db;
import niva.nl.rvo.external.ValidatorService;
import niva.nl.rvo.internal.PABackendService;
import org.json.simple.JSONObject;
import niva.nl.rvo.model.ECROPReturnMessage;
import niva.nl.rvo.model.Message;
import niva.nl.rvo.model.NIVAUc4bMessage;
import niva.nl.rvo.model.NIVAUc4bReturnMessage;
import niva.nl.rvo.services.cors.AcceptAll;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author rande001, rivie003
 */
    @WebServlet(name = "NivaECropMessage", urlPatterns = {"/nivaecropmessage"})
    public class NivaECropMessage extends HttpServlet {
    /**
     * Processes requests (JSON) for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request, body: {getECROPReportMessage: {...}}
     * @param response servlet response: ECROPReturnMessage: {...}
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //output
        response.setContentType("application/json;charset=UTF-8");
        NIVAUc4bReturnMessage returnMessage = new NIVAUc4bReturnMessage();
        ArrayList<Message> messages = new ArrayList();
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        // in/output
        JSONParser parser = new JSONParser();
        // input
        StringBuilder jsonMessage = new StringBuilder();
        String username = "anonymous", statusCode = "";
        
        try {
            returnMessage.setECROPReturnMessage(new ECROPReturnMessage(0, "Ok"));
            //
            // Parse json. Enters via POST.
            //
            BufferedReader b = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String tmp = b.readLine();
            while (tmp != null) {
                jsonMessage.append(tmp);
                tmp = b.readLine();
            }
            b.close();

            System.out.println("check request empty");
            if (jsonMessage.toString().trim().length() == 0) {
                messages.add(new Message("","Error",400,"No data, empty body"));
                returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                jsonMessage.append("{\"body\":null}");   // prevent SQL error
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            //
            // JSON Object and not empty? 
            //
            JSONObject jsonData = null;
            System.out.println("check JSON object");
            try {
                Object jsonobject = parser.parse(jsonMessage.toString());
		if (jsonobject instanceof JSONObject) {
                    jsonData = (JSONObject)jsonobject;
                    if (jsonData.isEmpty() 
                      || jsonData.get("ECROPReportMessage") == null 
                      || !(jsonData.get("ECROPReportMessage") instanceof JSONObject)
                      || ((JSONObject)(jsonData.get("ECROPReportMessage"))).isEmpty()
                       ) {
                        messages.add(new Message("","Error",400,"Empty (or insufficient) request"));
                        returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                        return;
                    }
                }
                else {
                    messages.add(new Message("","Error",400,"No JSONObject in body"));
                    returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                    jsonMessage.insert(0, "{\"body\":\"");     // prevent SQL error
                    jsonMessage.append("\"}");
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
                messages.add(new Message("","Error",400,"ParseError"));
                returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                jsonMessage.insert(0, "{\"body\":\"");     // prevent SQL error
                jsonMessage.append("\"}");
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            
            //System.out.println("NivaECropMessage: ".concat(jsonData.toJSONString()));
            //messages.add(new Message("","Debug",0,"JSON Ok"));
            
            //
            // conform Sample NIVAUc4bMessage? 
            //
            System.out.println("map to niva classes");
            try {
                ObjectMapper mapper = new ObjectMapper();
                NIVAUc4bMessage message= mapper.readValue(jsonMessage.toString(), NIVAUc4bMessage.class);
                
                try {
                    System.out.println("minimal check CropReportDocument.StatusCode (T/F)");
                    statusCode = message.getECROPReportMessage().getCropReportDocument().getStatusCode();
                    System.out.println("CropReportDocument.StatusCode: " + statusCode);
                } catch (Exception eee) {
                    System.out.println(eee.getLocalizedMessage());
                    messages.add(new Message("","Error",400,"Minimal report check failed: "+eee.getLocalizedMessage()));
                    returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    return;
                }
            } catch (Exception ee) {
                System.out.println(ee.getLocalizedMessage());
                messages.add(new Message("","Error",400,"Parse to NIVA failed: " +ee.getLocalizedMessage()));
                returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            //messages.add(new Message("","Debug",0,"Received Ok"));
            
            //
            // call external validator service
            //
            ECROPReturnMessage returnWoWOErrors;
            String validatorResult;
            System.out.println("call validator");
            try {
                ValidatorService validator = new ValidatorService();
                validatorResult = validator.process(jsonMessage.toString());
            } catch (Exception ee) {
                System.out.println(ee.getLocalizedMessage());
                messages.add(new Message("","Error",400,"Calling validator service failed"));
                returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                return;
            }
                
            System.out.println("map validation result");
            try {
                // "ReturnCode": 0 = No errors, 2 = Info Messages present, 5 = Warnings, 9 = errors encountered
                // "ReturnMessage": ['Received Ok', 'Validated Ok' (for ReturnCode 0), 'Warnings', 'Errors encountered']
                ObjectMapper mapper = new ObjectMapper();
                //validatorResult = validatorResult.replace("\"No_errors\"", "0");
                returnWoWOErrors = ((NIVAUc4bReturnMessage)mapper.readValue(validatorResult, NIVAUc4bReturnMessage.class)).getECROPReturnMessage();
                returnMessage.setECROPReturnMessage(returnWoWOErrors);
                if (returnWoWOErrors.getReturnCode() > 0) {
                    return;
                }
                else {
                    returnMessage.getECROPReturnMessage().setReturnMessage("Validated Ok");
                        
                    if (returnWoWOErrors.getMessageCount() > 0)  // preserve validator messages
                        messages.addAll(returnWoWOErrors.getMessages());
                }
            } catch (Exception ee) {
                System.out.println(ee.getLocalizedMessage());
                messages.add(new Message("","Error",400,"Parsing validator response failed"));
                returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                return;
            }
            //messages.add(new Message("","Debug",0,"Validation Ok"));
            
            // only allowed on StatusCode F, not on T or unknown
            if (statusCode.equals("F")) {
                //
                // call backend service
                //
                String backendResult;
                System.out.println("call backend");
                try {
                    PABackendService backend = new PABackendService();
                    backendResult = backend.process(jsonData);
                } catch (Exception ee) {
                    System.out.println(ee.getLocalizedMessage());
                    messages.add(new Message("","Error",400,"Calling backend service failed"));
                    returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                    return;
                }

                System.out.println("map backend result");
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    returnWoWOErrors = ((NIVAUc4bReturnMessage)mapper.readValue(backendResult, NIVAUc4bReturnMessage.class)).getECROPReturnMessage();
                    returnMessage.setECROPReturnMessage(returnWoWOErrors);
                    if (returnWoWOErrors.getReturnCode() > 0) {
                        return;
                    }
                    else {
                        returnMessage.getECROPReturnMessage().setReturnMessage("Received Ok");
                        
                        if (returnWoWOErrors.getMessageCount() > 0)
                            messages.addAll(returnWoWOErrors.getMessages());
                    }
                } catch (Exception ee) {
                    System.out.println(ee.getLocalizedMessage());
                    messages.add(new Message("","Error",400,"Parsing backend response failed: "+ee.getLocalizedMessage()));
                    returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                    return;
                }
                //messages.add(new Message("","Debug",0,"Backend Ok"));
            }
            
            //
            System.out.println("ready");
            if (messages.size() > 0)
                returnMessage.getECROPReturnMessage().setMessages(messages);
        } catch (Exception e) {
            try {
                messages.add(new Message("","Error",400,"Error outer: ".concat(e.getLocalizedMessage())));
                returnMessage.setECROPReturnMessage(new ECROPReturnMessage(9, "Errors Encoutered", messages));
                return;
            } catch (Exception ee) {
                Logger.getLogger(NivaECropMessage.class.getName()).log(Level.SEVERE, null, ee);
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
            return;
        }
        finally {
            PrintWriter out = response.getWriter();
            String result = "";
            Db db = new Db();
            
            try {
                result = ((JSONObject)parser.parse(ow.writeValueAsString(returnMessage))).toJSONString();
               
                // NLUMDG-106 validation only or not?
                if (statusCode.equals("F")) {
                    java.sql.Connection con = null;
                    // get the encoded username, else use referer (client) or anonymous
                    // @ToDo: test bericht of definitief bericht?
                    String authHeader = request.getHeader("Authorization");
                    if (authHeader != null && authHeader.startsWith("Basic ")) {
                        String encodedUsernamePassword = authHeader.substring("Basic ".length()).trim();
                        String usernamePassword =  new String( Base64.getDecoder().decode(encodedUsernamePassword.getBytes()), StandardCharsets.UTF_8);
                        username = usernamePassword.split(":")[0];
                    }
                    if (username == null) username = request.getHeader("Referer");
                    if (username == null) username = "anonymous";

                    con = db.getConnection();
                    //Issuer, ECropReportMessage, ECropReturnMessage
                    db.saveCropReport(con, username, jsonMessage.toString(), result);

                    // double close
                    con.close();
                    db.close();
                }
                
            } catch (SQLException ee) {
                db.close();
                System.out.println("SQLException: "+ee.getLocalizedMessage());
            } catch (Exception ee) {
                db.close();
                System.out.println("Other: "+ee.getLocalizedMessage());
            }
            out.println(result);
            System.out.println("return Body (object): "+returnMessage.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Preflight.
     * @param req
     * @param resp
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //System.out.println("doOptions 1 " + req.getServletPath() + " " + req.getMethod());
        
        AcceptAll.setAccessControlHeaders(resp);
        
        //System.out.println("doOptions 2 " + resp.getHeader("Access-Control-Allow-Origin"));
    }
}